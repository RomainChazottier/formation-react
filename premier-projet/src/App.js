import React, { Component, Fragment } from 'react';
import './App.css';
import Membre from './components/Membre'
import Button from './components/Button'

const famille = {
  membre1: {
    nom: 'Antho',
    age : 27
  },
  membre2: {
    nom: 'Ségolène',
    age : 27
  },
  membre3: {
    nom: 'Eléanor',
    age : 0
  },
  membre4: {
    nom: 'Mocha',
    age : 2
  }
}

class App extends Component {
  state = {
    famille,
    isShow: false
  }

  handleClick = (num) => {
    const famille = { ... this.state.famille }
    famille.membre1.age += num
    this.setState({ famille })
  }

  handleChange = (event, id) => {
    const famille = { ... this.state.famille }
    const nom = event.target.value
    famille[id].nom = nom
    this.setState({ famille })
  }

  hideName = id => {
    const famille = { ... this.state.famille }
    famille[id].nom = 'X'
    this.setState({ famille })
  }

  handleShowDescription = () => {
    const isShow = !this.state.isShow
    this.setState({ isShow })
  }

  render () {
    const { titre } = this.props
    const { famille, isShow } = this.state

    let description = null    
    
    if (isShow) {
      description = <strong>Je suis un chien.</strong>
    }

    const liste = Object.keys(famille)
      .map(membre => (
        <Membre 
          key= { membre }
          handleChange={event => this.handleChange(event, membre)}
          hideName={() => this.hideName(membre)}
          age={famille[membre].age}
          nom={famille[membre].nom}/>
      ))

    return (    
      <div className="App">
        <h1>{titre}</h1>        
        { liste }
        { /* <Membre 
          age={famille.membre4.age}
          nom={famille.membre4.nom}>
          { description }
          <button onClick={ this.handleShowDescription }>
            {
              isShow ? 'Cacher' : 'Montrer'            
            }
            </button>
          </Membre> */}  
          <hr/>
        <Button
          vieillir = { () => this.handleClick(2) } />      
      </div>
    )
  }
}

export default App
