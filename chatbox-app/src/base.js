import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDeUPCSnwkx4VX2cnaW75p-j1FuEocccVs",
    authDomain: "chatbox-app-24dd1.firebaseapp.com",
    databaseURL: "https://chatbox-app-24dd1.firebaseio.com",
})

const base = Rebase.createClass(firebase.database())

export { firebaseApp }

export default base