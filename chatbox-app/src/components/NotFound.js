import React from 'react'

const NotFound = () => (
    <h2 className='notFound'>Rien à trouver ici</h2>
)

export default NotFound
