import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyCqoPQGUJ25MY_iXFiqPd5q3L39XbtnCf0",
  authDomain: "recipes-app-46509.firebaseapp.com",
  databaseURL: "https://recipes-app-46509.firebaseio.com",
})

const base = Rebase.createClass(firebaseApp.database())

// This is a named export
export { firebaseApp }

// this is a default export
export default base
