import React from 'react'
import PropTypes from 'prop-types'
import './App.css' 

import Header from './components/Header'
import Admin from './components/Admin'
import Card from './components/Card'

import withFirebase from './components/hoc/withFirebase'

import ColorContext from './components/Color'

const App = (props) => {
  const cards = Object.keys(props.recettes)
      .map(key => <Card key={key} details={props.recettes[key]}/>)

    return (
      <ColorContext>
        <div className='box'>
          <Header pseudo={props.match.params.pseudo}/>
          <div className='cards'>
            {cards}
          </div>
          <Admin
            pseudo={props.match.params.pseudo}
            recettes={props.recettes}
            majRecette={props.majRecette}
            supprimerRecette={props.supprimerRecette}
            ajouterRecette = {props.ajouterRecette}
            chargerExemple={props.chargerExemple}/>
        </div>
      </ColorContext>
    )
}

App.propTypes = {
  match: PropTypes.object.isRequired,
  recettes: PropTypes.object.isRequired,
  ajouterRecette: PropTypes.func.isRequired,
  majRecette: PropTypes.func.isRequired,
  supprimerRecette: PropTypes.func.isRequired,
  chargerExemple: PropTypes.func.isRequired
}

const WrappedComponent = withFirebase(App)

export default WrappedComponent
